﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using zkemkeeper;
using System.Data.SqlClient;
using System.Collections;
using System.IO;
using System.Configuration;
namespace WindowsFormsApplication1
{

    public partial class Main : Form
    {
        public zkemkeeper.CZKEMClass drvReloj = new zkemkeeper.CZKEMClass();
        private int iMachineNumber = 1;

        private string IP_Device = AgenteBioAdmin.Properties.Settings.Default.IP_Device;
        private string Port_Device = AgenteBioAdmin.Properties.Settings.Default.Port_Device;
        private string File_Dir = AgenteBioAdmin.Properties.Settings.Default.File_Dir;
        private string File_Name = AgenteBioAdmin.Properties.Settings.Default.File_Name;
        private string Delimitador = AgenteBioAdmin.Properties.Settings.Default.Separator;
        private FSLogger log;
        public Main()
        {
            string Linea;
            try
            { 
                log = new FSLogger(System.Environment.CurrentDirectory + "\\log.txt");
                log.EscribirLog("Inicio del proceso");

                string FilePath = File_Dir + "\\" + File_Name;


                if (AgenteBioAdmin.Properties.Settings.Default.DeleteFile)
                {
                    if (File.Exists(FilePath))
                    {
                        File.Delete(FilePath);
                    }
                }

            
                InitializeComponent();
                Directory.SetCurrentDirectory(File_Dir);

                using (FileStream Archivo = File.Open(FilePath, FileMode.Append, FileAccess.Write, FileShare.Write))
                {
                    using (StreamWriter Escritura = new StreamWriter(Archivo))
                    {
                        //using (StreamWriter Escritura = File.AppendText(File_Dir)) {

                        log.EscribirLog("Archivo Creado");
                        //10.10.1.75
                        //IP_Device = "10.10.1.75";
                        if (drvReloj.Connect_Net(IP_Device, int.Parse(Port_Device)))
                        //drvReloj.SetCommuTimeOut(60);
                        //if (drvReloj.Connect_Net("200.84.153.185", 4370))
                        {
                            string sdwEnrollNumber = "";
                            int idwVerifyMode = 0;
                            int idwInOutMode = 0;
                            int idwYear = 0;
                            int idwMonth = 0;
                            int idwDay = 0;
                            int idwHour = 0;
                            int idwMinute = 0;
                            int idwSecond = 0;
                            int idwWorkcode = 0;

                            int idwErrorCode = 0;
                            int iGLCount = 0;
                            int iIndex = 0;

                            DateTime Today = DateTime.Now;
                            DateTime Days = Today.AddDays(-30);
                            DateTime Dia_a_Comparar;
                            double DiferenciaDeDias;

                            Cursor = Cursors.WaitCursor;
                            drvReloj.EnableDevice(iMachineNumber, false);//disable the device
                            log.EscribirLog("Inicio la lectura de registros del Biometrico");
                            if (drvReloj.ReadGeneralLogData(iMachineNumber))//read all the attendance records to the memory
                            {
                                while (drvReloj.SSR_GetGeneralLogData(iMachineNumber, out sdwEnrollNumber, out idwVerifyMode, out idwInOutMode, out idwYear, out idwMonth, out idwDay, out idwHour, out idwMinute, out idwSecond, ref idwWorkcode))//get records from the memory
                                {
                                    Dia_a_Comparar = DateTime.Parse(idwYear.ToString() + "/" + idwMonth.ToString() + "/" + idwDay.ToString() + " " + idwHour.ToString() + ":" + idwMinute.ToString() + ":" + idwSecond.ToString());
                                    DiferenciaDeDias = ((Today - Dia_a_Comparar)).TotalDays;
                                    //DiferenciaDeDias = Dia_a_Comparar
                                    //DiferenciaDeDias=System.Data.SqlClient.
                                    // da = TimeSpan.Parse(DiferenciaDeDias.ToString()).Days;
                                    if (DiferenciaDeDias <= 30 && DiferenciaDeDias > 0)
                                    {
                                        iGLCount++;
                                        Linea = sdwEnrollNumber + Delimitador + idwYear.ToString() + "/" + idwMonth.ToString("00") + "/" + idwDay.ToString("00") + " " + idwHour.ToString("00") + ":" + idwMinute.ToString("00") + ":" + idwSecond.ToString("00");
                                        if (Linea != "" || Linea.Length != 0 || Linea != " ")
                                        {
                                            Escritura.WriteLine(Linea);
                                            iIndex++;
                                        }
                                    }
                                }
                                log.EscribirLog("Datos Descargados");
                            }
                            else
                            {
                                Cursor = Cursors.Default;
                                drvReloj.GetLastError(ref idwErrorCode);

                                if (idwErrorCode != 0)
                                {
                                    //MessageBox.Show("Reading data from terminal failed,ErrorCode: " + idwErrorCode.ToString(), "Error");
                                    log.EscribirLog("Fallo en la lenctura de datos del dispositivo: " + idwErrorCode.ToString());
                                }
                                else
                                {
                                    //MessageBox.Show("No data from terminal returns!", "Error");
                                    log.EscribirLog("No hay datos en el dispositvo a importar");
                                }
                            }
                            drvReloj.EnableDevice(iMachineNumber, true);//enable the device
                            Cursor = Cursors.Default;

                        }
                        else
                        {
                            log.EscribirLog("No se puede acceder al dispositivo");
                        };
                        log.EscribirLog("Proceso terminado");
                    }
                }
                
               
            }
            catch(Exception ex)
            {
                log.EscribirLog(ex);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
